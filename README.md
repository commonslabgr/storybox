# storybox

![alt text](templates/logos/logo_storybox.svg "StoryBox Logo")

StoryBox is a collaborative art project created by [CommonsLab](https://commonslab.gr) and [Openbook](https://openbook.gr)
It is an open source project that prints short stories. 

It uses a Raspberry Pi and a thermal printer, the stories are all public domain or Creative Commons licensed material.

**How to build your own**

You will need the hardware listed below, assemble it with the case and install the necessary software to print your stories.
The cost of materials is ~120€.

***Hardware***
* [Raspberry Pi](https://www.raspberrypi.org/) (0, 2,3 & 4) 
* Raspberry Pi PSU [grobotronics](https://grobotronics.com/5v-2.5a-raspberry-pi.html)
* micro SD Card (min 4GB) [e-shop.gr](https://www.e-shop.gr/ixos-eikona-kartes-mnimis-micro-sd-high-capacity-micro-sdhc-list?table=PER&category=%C5%C9%C4%C9%CA%C5%D3+%CC%CD%C7%CC%C5%D3&filter-1537=1)
* Thermal printer with USB (JP-QR204 58mm) [Adafruit](https://www.adafruit.com/product/2751)
* PSU 5-12V 2A for printer [Grobotronics](https://grobotronics.com/trofodotiko-12v-2a-output-5.5x2.1-psu-1602.html)
* Push Button [Grobotronics](https://grobotronics.com/metal-pushbutton-momentary-19mm-blue.html)
* USB cable (mini to A) [e-shop.gr](https://www.e-shop.gr/equip-128521-usb-20-cable-a-male-mini-18m-p-PER.750530)
* micro USB extension cable [e-shop.gr](https://www.e-shop.gr/delock-83567-extension-cable-usb-20-type-micro-b-male-usb-20-type-micro-b-female-05m-p-PER.611766)
* Case [download STL files]()

***3D Print your own case***

**Software**
[Download](https://www.raspberrypi.org/downloads/raspbian/) latest Raspbian image and install it on your micro SD card. Follow the [installation guide](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).

One you have Raspbian up an running on your Pi you need to follow the instructions below to install the ZJ-58 driver for the printer and then install the storybox software.

***Install printer driver***
```
    sudo apt-get update
    sudo apt-get install libcups2-dev libcupsimage2-dev git build-essential cups system-config-printer
```
```
    git clone https://github.com/adafruit/zj-58
    cd zj-58
    make
    sudo ./install
```
***Install storybox***

Download the code and place it in /home/pi/storybox

edit /etc/xdg/lxsession/LXDE-pi/autostart

add the following line at the end of the file
```
/usr/bin/python /home/pi/storybox/run.py
```

Add the stories you want in pdf format in the /home/pi/storybox/stories folder.

You can use the templates we have in the templates folder which have the printer's paper size.

**Add your own stories**

You can use our templates [here](templates/). We have preapared one for short stories, one for medium length and one for long stories but you can adjust their size by changing the height of the page.

Then simply edit the text and export the documents in PDF format.
Copy the PDF stories that you want storybox to print to the Pi at /home/pi/storybox/stories folder. 
Remember to delete any stories files that you do not want the storybox to print.
import RPi.GPIO as GPIO
import time
import subprocess
import os, random
import logging

#Configuration
printerName = "ZJ-58"
storiesFolder = "/home/pi/storybox/stories"

#Set Pi GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

#create logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
loghandler = logging.FileHandler("storybox.log")
loghandler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
loghandler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(loghandler)

def printRandomStory():
    filename = random.choice(os.listdir(storiesFolder))
    print("Printing "+filename)
    logger.info("Printing "+filename)
    subprocess.run(["lp","-d","ZJ-58", storiesFolder+"/"+filename])


#Print a random story on start up to indicate that device is ready
printRandomStory()
time.sleep(2)

while True:
    if GPIO.input(10) == GPIO.HIGH:
        print("Button pressed")
        logger.info("Button pressed")
        printRandomStory()
        time.sleep(2)
    
